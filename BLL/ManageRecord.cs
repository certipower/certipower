﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ManageRecord
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public bool AddRecord(Record record)
        {
            context.Records.Add(record);
            context.SaveChanges();
            return true;
        }
        public bool UpdateRecord(Record record)
        {
            Record oldRecord = context.Records.FirstOrDefault(s => s.Id == record.Id);
            if (oldRecord != null)
            {
                oldRecord.FileName = record.FileName;
                oldRecord.FileDescription = record.FileDescription;
                oldRecord.FileType = record.FileType;
                oldRecord.FolderId = record.FolderId;
                oldRecord.InvoiceAmount = record.InvoiceAmount;
                oldRecord.Priority = record.Priority;
                oldRecord.ReceiveFrom = record.ReceiveFrom;
                oldRecord.SentTo = record.SentTo;
                oldRecord.OriginalFileName = record.OriginalFileName;
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public List<FetchRecord_Result> FetchRecords(Paging paging, SearchDepartment search)
        {
            return context.FetchRecord(search.Name, search.FolderId, search.OrganizationId, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
        }
        public bool Delete(int Id)
        {
            Record Record = context.Records.FirstOrDefault(s => s.Id == Id);
            if (Record != null)
            {
                context.Records.Remove(Record);
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public Record FetchRecordById(int Id)
        {
            return context.Records.FirstOrDefault(s => s.Id == Id);
        }
    }
}
