﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ManageAgreement
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public bool AddAgreement(Agreement agreement)
        {
            context.Agreements.Add(agreement);
            context.SaveChanges();
            return true;
        }
        public Agreement FetchAgreementById(int Id)
        {
            return context.Agreements.Where(s => s.Id == Id).FirstOrDefault();
        }
        public bool UpdateAgreement(Agreement agreement)
        {
            Agreement oldAgreement = context.Agreements.Where(s => s.Id == agreement.Id).FirstOrDefault();
            OrgInstituteAgreement oldInsOrgAgreement = context.OrgInstituteAgreements.Where(s => s.AgreementId == agreement.Id).FirstOrDefault();
            if (oldInsOrgAgreement != null)
            {
                oldInsOrgAgreement.OrganizationId = agreement.OrganizationId;
                oldInsOrgAgreement.InstituteId = agreement.InstituteId;
            }
            if (oldAgreement != null)
            {
                oldAgreement.PCEmail = agreement.PCEmail;
                oldAgreement.PCFullName = agreement.PCFullName;
                oldAgreement.PCIDExpiryDate = agreement.PCIDExpiryDate;
                oldAgreement.PCIDNumber = agreement.PCIDNumber;
                oldAgreement.PCMobileNo = agreement.PCMobileNo;
                oldAgreement.ProgramContent = agreement.ProgramContent;
                oldAgreement.ProgramName = agreement.ProgramName;
                oldAgreement.ProgramObjective = agreement.ProgramObjective;
                oldAgreement.ProgramType = agreement.ProgramType;
                oldAgreement.SystemAvailability = agreement.SystemAvailability;
                oldAgreement.TeachingMethod = agreement.TeachingMethod;
                oldAgreement.TraineeNumber = agreement.TraineeNumber;
                oldAgreement.TrainingCenterApprovalLetter = agreement.TrainingCenterApprovalLetter;
                oldAgreement.TrainingFeePerStudent = agreement.TrainingFeePerStudent;
                oldAgreement.TrainingMaterialAvailability = agreement.TrainingMaterialAvailability;
                oldAgreement.TrainingPlan = agreement.TrainingPlan;
                oldAgreement.InstructorAvailability = agreement.InstructorAvailability;
                oldAgreement.JobTitle = agreement.JobTitle;
                oldAgreement.Evaluation = agreement.Evaluation;
                oldAgreement.Duration = agreement.Duration;
                oldAgreement.Certification = agreement.Certification;
                oldAgreement.ApprovalBody = agreement.ApprovalBody;
                oldAgreement.PurchaseOrderNo = agreement.PurchaseOrderNo;
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public List<FetchAgreement_Result> FetchAgreements(Paging paging, SearchDepartment search)
        {
            return context.FetchAgreement(search.Name, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
        }
        public Agreement FetchAgreementStudentCount(int agreementId)
        {
            return context.Agreements.Where(s => s.Id == agreementId).FirstOrDefault();
        }
    }
}
