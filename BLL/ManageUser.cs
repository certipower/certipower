﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ManageUser
    {
        public int AddUser(User user)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                context.Users.Add(user);
                context.SaveChanges();
                return user.Id;
            }
        }

        public bool UpdateUser(User user)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                User oldUser = context.Users.FirstOrDefault(s => s.Id == user.Id);
                if (oldUser != null)
                {
                    oldUser.FullName = user.FullName;
                    oldUser.Email = user.Email;
                    oldUser.DateOfBirth = user.DateOfBirth;
                    oldUser.Gender = user.Gender;
                    oldUser.IsActive = user.IsActive;
                    oldUser.Nationality = user.Nationality;
                    oldUser.Picture = user.Picture;
                    oldUser.JobTitle = user.JobTitle;
                    oldUser.Password = user.Password;
                    oldUser.Notes = user.Notes;
                    oldUser.Phone = user.Phone;
                    oldUser.GroupId = user.GroupId;
                    oldUser.OriginalPicture = user.OriginalPicture;
                    try
                    {

                        context.SaveChanges();
                        return true;
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    //context.SaveChanges();
                    //return true;
                }
                return false;
            }
        }

        public List<FetchUser_Result> FetchUsers(Paging paging, SearchDepartment search)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                return context.FetchUser(search.Name, search.Status, search.OrganizationId, search.GroupId, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
            }
        }

        public User FetchUserById(int userId)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                return context.Users.FirstOrDefault(s => s.Id == userId);
            }
        }
        public bool Delete(int Id)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                User user = context.Users.FirstOrDefault(s => s.Id == Id);
                if (user != null)
                {
                    context.Users.Remove(user);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public bool UpdateProfile(User user)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                User oldUser = context.Users.FirstOrDefault(s => s.Id == user.Id);
                if (oldUser != null)
                {
                    oldUser.FullName = user.FullName;
                    //oldUser.Email = user.Email;
                    //oldUser.DateOfBirth = user.DateOfBirth;
                    oldUser.Gender = user.Gender;
                    oldUser.IsActive = user.IsActive;
                    oldUser.Nationality = user.Nationality;
                    oldUser.Picture = user.Picture;
                    oldUser.JobTitle = user.JobTitle;
                    //oldUser.Password = user.Password;
                    oldUser.Notes = user.Notes;
                    oldUser.Phone = user.Phone;
                    //oldUser.GroupId = user.GroupId;
                    oldUser.OriginalPicture = user.OriginalPicture;
                    try
                    {
                        context.SaveChanges();
                        return true;
                    }
                    catch{}
                }
                return false;
            }
        }
    }
}
