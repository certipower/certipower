﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Common
    {

    }
    public class Paging
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string sortField { get; set; }
        public string sortOrder { get; set; }
        public string active { get; set; }
    }

    public class SearchDepartment
    {
        public string Name { get; set; }
        public String Status { get; set; }
        public string Email { get; set; }
        public string CNIC { get; set; }
        public string Code { get; set; }
        public int GroupId { get; set; }
        public int FolderId { get; set; }
        public int OrganizationId { get; set; }
    }
    public class DropDownList
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public enum PriorityLevel
    {
        Critical = 0,
        High = 1,
        Medium = 2,
        Low = 3
    }

    public enum Gender
    {
        Male = 0,
        Female = 1
    }
    public enum OrganizationSize
    {
        [Description("1-100")]
        Hundred = 1,
        [Description("101-500")]
        FiveHundred = 2,
        [Description("501-1000")]
        Thousand = 3,
        [Description("1001-1500")]
        FifteenHundred = 4,
        [Description("1501-2000")]
        TwoThousand = 5
    }
    public enum OrganizationType
    {
        [Description("Organization")]
        Organization = 1,
        [Description("Individual Company")]
        IndividualCompany = 2
    }

    public enum InstituteType
    {
        [Description("Institute")]
        Institute = 1,
        [Description("Individual Institute")]
        IndividualInstitute = 2
    }
    public enum UserRoles
    {
        Admin = 1,
        ChatTeam = 7,
        Financier = 4,
        Organization = 5,
        User = 6
    }
}
