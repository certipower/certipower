﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ManageFolder
    {
        public bool AddFolder(Folder folder)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                context.Folders.Add(folder);
                context.SaveChanges();
                return true;
            }
        }
        public bool UpdateFolder(Folder folder)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                Folder oldFolder = context.Folders.FirstOrDefault(s => s.Id == folder.Id);
                if (oldFolder != null)
                {
                    oldFolder.Name = folder.Name;
                    oldFolder.IsActive = folder.IsActive;
                    oldFolder.Description = folder.Description;
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }

        public List<FetchFolder_Result> FetchFolders(Paging paging, SearchDepartment search)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                return context.FetchFolder(search.Name, search.Status,search.OrganizationId, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
            }
        }
        public bool Delete(int Id)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                Folder folder = context.Folders.FirstOrDefault(s => s.Id == Id);
                if (folder != null)
                {
                    context.Folders.Remove(folder);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public List<DropDownList> FetchFolder(int organizationId)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                return context.Folders.Where(i => i.IsActive == true && i.OrganizationId == organizationId).Select(s => new DropDownList() { Id = s.Id, Name = s.Name }).ToList();
            }
        }
    }
}
