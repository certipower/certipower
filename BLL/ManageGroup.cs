﻿using DAL;
using System.Linq;
using System;
using System.Collections.Generic;

namespace BLL
{
    public class ManageGroup
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public bool AddGroup(Group group)
        {
            context.Groups.Add(group);
            context.SaveChanges();
            return true;
        }
        public bool UpdateGroup(Group group)
        {
            context.UpdateGroupUsers(group.Id, group.PrivilageId, group.Id, group.Name, group.Description, group.IsActive);
            return true;
        }
        public List<FetchGroup_Result> FetchGroups(Paging paging, SearchDepartment search)
        {
            return context.FetchGroup(search.Name, search.Status, search.OrganizationId, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
        }
        public bool Delete(int Id)
        {
            Group group = context.Groups.FirstOrDefault(s => s.Id == Id);
            if (group != null)
            {
                context.Groups.Remove(group);
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public List<DropDownList> FetchGroup(int organizationId)
        {
            return context.Groups.Where(i => i.IsActive == true && i.OrganizationId == organizationId).Select(s => new DropDownList() { Id = s.Id, Name = s.Name }).ToList();
        }
    }
}
