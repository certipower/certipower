﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ManagePrivilage
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public List<DropDownList> FetchPrivilages()
        {
            return context.AspNetRoles.Select(s => new DropDownList() { Id = s.Id, Name = s.Name }).ToList();
        }
    }
}
