﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ManageStudent
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public bool AddUpdateStudent(List<Student> lstStudent)
        {
            if (lstStudent.Count == lstStudent.Where(s => s.Id == 0).Count())
            {
                context.Students.AddRange(lstStudent);
            }
            else
            {
                int agreementId = lstStudent.FirstOrDefault().AgreementId;
                List<Student> lstOldStudent = context.Students.Where(s => s.AgreementId == agreementId).ToList();
                foreach (var item in lstStudent)
                {
                    Student std = lstOldStudent.FirstOrDefault(s => s.Id == item.Id);
                    if (std != null)
                    {
                        std.Name = item.Name;
                        std.FatherName = item.FatherName;
                        std.CNIC = item.CNIC;
                        std.IsActive = item.IsActive;
                    }
                    else
                    {
                        context.Students.Add(item);
                    }
                }
            }
            context.SaveChanges();
            return true;
        }
    }
}
