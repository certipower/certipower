﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ManageOrganization
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public int AddOrganization(Organization org)
        {
            context.Organizations.Add(org);
            context.SaveChanges();
            return org.Id;
        }
        public bool UpdateOrganization(Organization org)
        {
            Organization oldOrg = context.Organizations.FirstOrDefault(s => s.Id == org.Id);
            if (oldOrg != null)
            {
                oldOrg.OrganizationName = org.OrganizationName;
                oldOrg.OrganizationSize = org.OrganizationSize;
                oldOrg.OrganizationType = org.OrganizationType;

                oldOrg.AddressStreetArea = org.AddressStreetArea;

                oldOrg.BankAccountDetailLetter = org.BankAccountDetailLetter;
                oldOrg.BankName = org.BankName;

                oldOrg.CommercialRecordExpiryDate = org.CommercialRecordExpiryDate;
                oldOrg.CommercialRecordNo = org.CommercialRecordNo;

                oldOrg.Email = org.Email;
                oldOrg.EstablishDate = org.EstablishDate;

                oldOrg.Fax = org.Fax;
                oldOrg.GMEmail = org.GMEmail;
                oldOrg.GMFullName = org.GMFullName;
                oldOrg.GMMobileNo = org.GMMobileNo;
                oldOrg.IBAN = org.IBAN;
                oldOrg.IDExpiryDate = org.IDExpiryDate;
                oldOrg.IDNumber = org.IDNumber;

                oldOrg.IssuingAuthority = org.IssuingAuthority;

                oldOrg.MobileNo = org.MobileNo;

                oldOrg.POBox = org.POBox;

                oldOrg.Telephone = org.Telephone;

                oldOrg.ZipCode = org.ZipCode;
                oldOrg.IsActive = org.IsActive;
                oldOrg.NoOfUserAllowed = org.NoOfUserAllowed;
                oldOrg.UserName = org.UserName;
                oldOrg.Password = org.Password;
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public Organization FetchById(int Id)
        {
            return context.Organizations.FirstOrDefault(s => s.Id == Id);
        }
        public bool DeleteOrganization(int Id)
        {
            Organization org = context.Organizations.FirstOrDefault(s => s.Id == Id);
            if (org != null)
            {
                context.Organizations.Remove(org);
                context.SaveChanges();
                return true;
            }
            return false;
        }
        public List<FetchOrganization_Result> FetchOrganizations(Paging paging, SearchDepartment search)
        {
            using (CertiPowerEntities context = new CertiPowerEntities())
            {
                return context.FetchOrganization(search.Name, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
            }
        }

        public List<DropDownList> FetchOrganizations()
        {
            return context.Organizations.Where(s => s.IsActive == true && s.IsOrganization == true).Select(n => new DropDownList() { Id = n.Id, Name = n.OrganizationName }).ToList();
        }
        public List<DropDownList> FetchAgreementById(int organizationId)
        {
            List<DropDownList> lstAgreement = context.Agreements.Where(s => s.OrgInstituteAgreements.Any(n => n.OrganizationId == organizationId) || s.OrgInstituteAgreements.Any(n => n.InstituteId == organizationId)).Select(m => new DropDownList() { Id = m.Id, Name = m.PCFullName }).ToList();
            return lstAgreement;
        }

       
    }
}
