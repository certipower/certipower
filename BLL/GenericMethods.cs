﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class GenericMethods
    {
        public CertiPowerEntities context = new CertiPowerEntities();
        public List<DropDownList> FetchPriority()
        {
            List<DropDownList> lstStatus = new List<DropDownList>();
            DropDownList status;
            foreach (PriorityLevel item in Enum.GetValues(typeof(PriorityLevel)))
            {
                status = new DropDownList();
                status.Id = (int)item;
                status.Name = item.ToString();
                lstStatus.Add(status);
            }
            return lstStatus;
        }
        public List<DropDownList> FetchGender()
        {
            List<DropDownList> lstStatus = new List<DropDownList>();
            DropDownList status;
            foreach (Gender item in Enum.GetValues(typeof(Gender)))
            {
                status = new DropDownList();
                status.Id = (int)item;
                status.Name = item.ToString();
                lstStatus.Add(status);
            }
            return lstStatus;
        }
        public List<DropDownList> FetchCompanySize()
        {
            List<DropDownList> lstStatus = new List<DropDownList>();
            DropDownList status;
            foreach (OrganizationSize item in Enum.GetValues(typeof(OrganizationSize)))
            {
                status = new DropDownList();
                status.Id = (int)item;
                status.Name = Extension.GetStatusDescription(item);
                lstStatus.Add(status);
            }
            return lstStatus;
        }
        public List<DropDownList> FetchOrganziationType()
        {
            List<DropDownList> lstOrgType = new List<DropDownList>();
            DropDownList orgType;
            foreach (OrganizationType item in Enum.GetValues(typeof(OrganizationType)))
            {
                orgType = new DropDownList();
                orgType.Id = (int)item;
                orgType.Name = Extension.GetStatusDescription(item);
                lstOrgType.Add(orgType);
            }
            return lstOrgType;
        }

        public List<DropDownList> FetchInstituteType()
        {
            List<DropDownList> lstInstituteType = new List<DropDownList>();
            DropDownList instType;
            foreach (InstituteType item in Enum.GetValues(typeof(InstituteType)))
            {
                instType = new DropDownList();
                instType.Id = (int)item;
                instType.Name = Extension.GetStatusDescription(item);
                lstInstituteType.Add(instType);
            }
            return lstInstituteType;
        }
    }
    public static class Extension
    {
        public static FetchOrganizationDetail_Result OrganizationProfileFetch(this string id)
        {
            int userId = Convert.ToInt32(id);
            FetchOrganizationDetail_Result userProfile = new GenericMethods().context.FetchOrganizationDetail(userId).FirstOrDefault();
            return userProfile;
        }
        public static string GetStatusDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

    }
}
