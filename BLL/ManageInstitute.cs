﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class ManageInstitute
    {
        private CertiPowerEntities context = new CertiPowerEntities();
        public List<FetchInstitute_Result> FetchInstitutes(Paging paging, SearchDepartment search)
        {
            return context.FetchInstitute(search.Name, paging.pageSize, paging.pageIndex, paging.sortField, paging.sortOrder).ToList();
        }
        public List<DropDownList> FetchInstitute()
        {
            return context.Organizations.Where(s => s.IsActive == true && s.IsOrganization == false).Select(n => new DropDownList() { Id = n.Id, Name = n.OrganizationName }).ToList();
        }
       
    }
}
