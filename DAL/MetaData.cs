﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DAL
{
   public class MetaData
    {
    }

    #region Entity Framework Classes
    [MetadataType(typeof(UserMetadata))]
    public partial class User
    {
        public bool Active { get; set; }
    }

    [MetadataType(typeof(OrganizationMetadata))]
    public partial class Organization
    {
        public bool IsLogin { get; set; }
    }

    [MetadataType(typeof(AgreementMetadata))]
    public partial class Agreement
    {
        [Required]
        public int OrganizationId { get; set; }
        [Required]
        public int InstituteId { get; set; }
    }
    [MetadataType(typeof(RecordMetadata))]
    public partial class Record
    { }
    [MetadataType(typeof(StudentMetadata))]
    public partial class Student
    {}
    #endregion

    #region Modal Classes
    public class AgreementViewModel
    {
        public int AgreementId { get; set; }
        public List<Student> lstStudent { get; set; }
    }
    #endregion

    #region Metadata Classes
    public partial class UserMetadata
    {
        [Required(ErrorMessage ="Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        public string Email { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone must be numeric")]
        public string Phone { get; set; }
        [Required]
        public string Nationality { get; set; }
        [Required]
        public DateTime? DateOfBirth { get; set; }
    }
    public partial class OrganizationMetadata
    {
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^[a-zA-Z0-9]*$",ErrorMessage ="Only alphanumeric characters.")]
        public string IBAN { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Telephone must be numeric")]
        public string Telephone { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "ZipCode must be numeric")]
        public string ZipCode { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Fax must be numeric")]
        public string Fax { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "MobileNo must be numeric")]
        public string MobileNo { get; set; }
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        public string GMEmail { get; set; }


    }
    public partial class AgreementMetadata
    {
        [Required(ErrorMessage = "Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail address")]
        public string PCEmail { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "PCMobileNo must be numeric")]
        public string PCMobileNo { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "Duration must be numeric")]
        public Nullable<int> Duration { get; set; }
        [RegularExpression("^[0-9]*$", ErrorMessage = "TraineeNumber must be numeric")]
        public Nullable<int> TraineeNumber { get; set; }
        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Fees can't have more than 2 decimal places")]
        public Nullable<double> TrainingFeePerStudent { get; set; }
        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Amount can't have more than 2 decimal places")]
        public Nullable<double> TotalAmount { get; set; }
    }
    public partial class RecordMetadata
    {
        [Required]
        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Amount can't have more than 2 decimal places")]
        public Nullable<decimal> InvoiceAmount { get; set; }
    }
    public partial class StudentMetadata
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string FatherName { get; set; }
        [Required]
        public string CNIC { get; set; }
    }
    #endregion
}
