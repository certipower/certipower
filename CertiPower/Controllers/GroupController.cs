﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CertiPower.Controllers
{
    public class GroupController : Controller
    {

        // GET: Group
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Save(Group group)
        {
            string msg = string.Empty;
            group.OrganizationId = new Account().FetchOrganization().OrganizationId;
            if (group.Id == 0)
            {
                if (new ManageGroup().AddGroup(group))
                {
                    msg = "Group has been added successfully";
                }
            }
            else
            {
                if (new ManageGroup().UpdateGroup(group))
                {
                    msg = "Group has been update successfully.";
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int Id)
        {
            string msg = string.Empty;
            if (new ManageGroup().Delete(Id))
            {
                msg = "Group has been deleted successfully.";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            departSearch.OrganizationId = new Account().FetchOrganization().OrganizationId;
            object gridData;
            try
            {
                List<FetchGroup_Result> list = new ManageGroup().FetchGroups(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }
    }
}