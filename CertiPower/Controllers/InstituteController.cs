﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using DAL;
using BLL;
using CertiPower.Models;
using System.Threading.Tasks;

namespace CertiPower.Controllers
{
    public class InstituteController : Controller
    {
        // GET: Institute
        private ApplicationUserManager _userManager;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddInstitute(int? Id)
        {
            if (Id.HasValue)
            {
                Organization org = new ManageOrganization().FetchById(Id.Value);
                org.IsLogin = !string.IsNullOrEmpty(org.UserName) ? true : false;
                return View(org);
            }
            return View(new Organization());
        }
        [HttpPost]
        public async Task<ActionResult> AddInstitute(Organization org)
        {
            org.IsOrganization = false;
            if (org.Id != 0)
            {
                new ManageOrganization().UpdateOrganization(org);
                return RedirectToAction("Index", "Institute");
            }
            else
            {
                int orgId = new ManageOrganization().AddOrganization(org);
                if (org.IsLogin)
                {
                    User user = new DAL.User();
                    user.IsActive = true;
                    user.Email = org.UserName;
                    user.Password = org.Password;
                    //user.JobTitle = org.JobTitle;
                    user.OrganizationId = orgId;
                    user.FullName = org.OrganizationName;

                    int userId = new ManageUser().AddUser(user);
                    RegisterViewModel model = new RegisterViewModel();
                    model.Email = org.UserName;
                    model.Password = org.Password;
                    model.OrganizationId = org.Id;
                    model.UserId = userId;
                    model.RoleId = (int)UserRoles.Organization;
                    var newUser = new ApplicationUser { UserName = model.Email, Email = model.Email, UserId = model.UserId, IsLogin = false };
                    ApplicationUserRole role = new ApplicationUserRole();
                    role.RoleId = model.RoleId;
                    newUser.Roles.Add(role);
                    var result = await UserManager.CreateAsync(newUser, model.Password);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index", "Institute");
                    }
                }
                return RedirectToAction("Index", "Institute");
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            object gridData;
            try
            {
                List<FetchInstitute_Result> list = new ManageInstitute().FetchInstitutes(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }
    }
}