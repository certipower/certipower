﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CertiPower.Controllers
{
    public class GenericController : Controller
    {
        // GET: Generic
        public JsonResult FetchFolders()
        {
            return Json(new ManageFolder().FetchFolder(new Account().FetchOrganization().OrganizationId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchGroups()
        {
            return Json(new ManageGroup().FetchGroup(new Account().FetchOrganization().OrganizationId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchPrivilages()
        {
            return Json(new ManagePrivilage().FetchPrivilages(), JsonRequestBehavior.AllowGet);
        }
    }
}