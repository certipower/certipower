﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CertiPower.Controllers
{
    public class RecordController : Controller
    {
        // GET: Record
        public ActionResult Index()
        {
            Record model = new Record();
            return View(model);
        }

        [HttpPost]
        public ActionResult Save(Record record, HttpPostedFileBase postedFiles)
        {
            string msg = string.Empty;
            //if (string.IsNullOrEmpty(record.FileName))
            //{
            //    if (postedFiles == null || postedFiles.ContentLength == 0)
            //    {
            //        TempData["Error"] = "Please upload files.";
            //        return RedirectToAction("Index");
            //    }
            //}
            
            if (postedFiles != null && postedFiles.ContentLength > 0)
            {
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string extension = System.IO.Path.GetExtension(postedFiles.FileName);
                if (extension == ".pdf" || extension == ".doc" || extension == ".docx" || extension == ".xlsx")
                {
                    string fileName = timestamp + System.IO.Path.GetExtension(postedFiles.FileName);
                    postedFiles.SaveAs(Server.MapPath("~/App_Shared/Files/" + fileName));
                    record.FileName = fileName;
                    record.OriginalFileName = postedFiles.FileName;
                }
                else
                {
                    TempData["Error"] = "Only .pdf,.doc and .xlsx formats are allowed.";
                    return RedirectToAction("Index");
                }
            }
            record.Date = DateTime.Now;
            record.OrganizationId = new Account().FetchOrganization().OrganizationId;
            if (record.Id == 0)
            {
                if (new ManageRecord().AddRecord(record))
                {
                    msg = "Record has been added successfully";
                }
            }
            else
            {
                if (new ManageRecord().UpdateRecord(record))
                {
                    msg = "Record has been update successfully.";
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult FetchById(int recordId)
        {
            DAL.Record record = new ManageRecord().FetchRecordById(recordId);
            //record.Active = record.IsActive.HasValue ? record.IsActive.Value : false;
            return PartialView("_Record", record);
        }
        public JsonResult Delete(int Id)
        {
            string msg = string.Empty;
            if (new ManageRecord().Delete(Id))
            {
                msg = "Record has been deleted successfully.";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            departSearch.OrganizationId = new Account().FetchOrganization().OrganizationId;
            object gridData;
            try
            {
                List<FetchRecord_Result> list = new ManageRecord().FetchRecords(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult FetchFolders()
        //{
        //    return Json(new ManageFolder().FetchFolder(), JsonRequestBehavior.AllowGet);
        //}
        public ActionResult OpenModal()
        {
            return PartialView("_Record", new Record());
        }
        public FileResult DownLoadFile(string file)
        {
            string contentType = string.Empty;
            if (file.Contains(".pdf"))
            {
                contentType = "application/pdf";
            }
            else if (file.Contains(".doc") || file.Contains(".docx"))
            {
                contentType = "application/doc";
            }
            else if (file.Contains(".xlsx") || file.Contains(".xls"))
            {
                contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }
            return File(Server.MapPath("~/App_Shared/Files/" + file), contentType, file);
        }
    }
}