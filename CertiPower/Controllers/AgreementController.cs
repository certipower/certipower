﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CertiPower.Controllers
{
    public class AgreementController : Controller
    {
        // GET: Agreement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddAgreement(int? Id)
        {
            if(Id.HasValue)
            {
                Agreement agreement = new ManageAgreement().FetchAgreementById(Id.Value);
                agreement.OrganizationId = agreement.OrgInstituteAgreements.FirstOrDefault().OrganizationId.Value;
                agreement.InstituteId = agreement.OrgInstituteAgreements.FirstOrDefault().InstituteId.Value;
                return View(agreement);
            }
            return View();
        }

        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            object gridData;
            try
            {
                List<FetchAgreement_Result> list = new ManageAgreement().FetchAgreements(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddAgreement(Agreement agreement)
        {
            //org.IsOrganization = true;
            OrgInstituteAgreement newAgreement = new OrgInstituteAgreement();
            newAgreement.OrganizationId = agreement.OrganizationId;
            newAgreement.InstituteId = agreement.InstituteId;
            
            if (agreement.Id != 0)
            {
                agreement.OrgInstituteAgreements.Add(newAgreement);
                if (new ManageAgreement().UpdateAgreement(agreement))
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                agreement.OrgInstituteAgreements.Add(newAgreement);
                if (new ManageAgreement().AddAgreement(agreement))
                {
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult AgreementStudents()
        {
            AgreementViewModel model = new AgreementViewModel();
            List<Student> lstStudent = new List<Student>();
            model.lstStudent = lstStudent;
            return View(model);
        }

        [HttpPost]
        public ActionResult FetchStudents(int? Id)
        {
            int noOfStudents = 0;
            AgreementViewModel model = new AgreementViewModel();
            Agreement agr = new Agreement();
            List<Student> lstAgreementStudent = new List<Student>();
            if (Id.HasValue)
            {
                agr = new ManageAgreement().FetchAgreementStudentCount(Id.Value);
            }
            List<Student> lstStudent = new List<Student>();
            Student std;
            if (agr.Students != null && agr.Students.Count > 0)
            {
                lstAgreementStudent = agr.Students.Where(s => s.IsActive == true).ToList();
                foreach (var item in lstAgreementStudent)
                {
                    lstStudent.Add(item);
                }
            }

            int? remainingStudent = agr.TraineeNumber - agr.Students.Where(s => s.IsActive == true).Count();
            for (int i = 0; i < remainingStudent; i++)
            {
                std = new Student();
                lstStudent.Add(std);
            }
            
            model.lstStudent = lstStudent;
            return PartialView("_AgreementStudents", model);
        }

        [HttpPost]
        public ActionResult SaveStudents(AgreementViewModel model)
        {
            model.lstStudent.ForEach(s => s.AgreementId = model.AgreementId);
            if (new ManageStudent().AddUpdateStudent(model.lstStudent))
            {
                return RedirectToAction("AgreementStudents");
            }
            return View();
        }
    }
}