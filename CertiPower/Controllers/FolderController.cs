﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CertiPower.Controllers
{
    public class FolderController : Controller
    {
        // GET: Folder
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Save(Folder folder)
        {
            folder.OrganizationId = new Account().FetchOrganization().OrganizationId;
            string msg = string.Empty;
            if (folder.Id == 0)
            {
                if (new ManageFolder().AddFolder(folder))
                {
                    msg = "Folder has been added successfully";
                }
            }
            else
            {
                if (new ManageFolder().UpdateFolder(folder))
                {
                    msg = "Folder has been update successfully.";
                }
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int Id)
        {
            string msg = string.Empty;
            if (new ManageFolder().Delete(Id))
            {
                msg = "Folder has been deleted successfully.";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            departSearch.OrganizationId = new Account().FetchOrganization().OrganizationId;
            object gridData;
            try
            {
                List<FetchFolder_Result> list = new ManageFolder().FetchFolders(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }
    }
}