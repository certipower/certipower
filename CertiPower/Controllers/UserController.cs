﻿using BLL;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using CertiPower.Models;
using System.Threading.Tasks;

namespace CertiPower.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        private ApplicationUserManager _userManager;
        public ActionResult Index()
        {
            DAL.User model = new DAL.User();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Save(User user, HttpPostedFileBase postedFiles)
        {
            string msg = string.Empty;
            //if (string.IsNullOrEmpty(user.Picture))
            //{
            //    if (postedFiles == null || postedFiles.ContentLength == 0)
            //    {
            //        TempData["Error"] = "Please upload picture.";
            //        return RedirectToAction("Index");
            //    }
            //}
            FetchOrganizationDetail_Result org = new Account().FetchOrganization();

            if (postedFiles != null && postedFiles.ContentLength > 0)
            {
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string fileName = timestamp + System.IO.Path.GetExtension(postedFiles.FileName);
                postedFiles.SaveAs(Server.MapPath("~/App_Shared/Images/" + fileName));
                user.Picture = fileName;
                user.OriginalPicture = postedFiles.FileName;
            }
            user.OrganizationId = new Account().FetchOrganization().OrganizationId;
            user.IsActive = user.Active;

            if (user.Id == 0)
            {
                int userId = new ManageUser().AddUser(user);
                if (userId != 0)
                {
                    var newUser = new ApplicationUser { UserName = user.Email, Email = user.Email, UserId = userId, IsLogin = false };
                    ApplicationUserRole role = new ApplicationUserRole();
                    role.RoleId = (int)UserRoles.User;
                    newUser.Roles.Add(role);
                    var result = await UserManager.CreateAsync(newUser, user.Password);
                    msg = "User has been added successfully";
                }
            }
            else
            {
                if (new ManageUser().UpdateUser(user))
                {
                    msg = "User has been update successfully.";
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult FetchById(int userId)
        {
            DAL.User user = new ManageUser().FetchUserById(userId);
            user.Active = user.IsActive.HasValue ? user.IsActive.Value : false;
            return PartialView("_User", user);
        }
        [HttpGet]
        public ActionResult OpenModal()
        {
            return PartialView("_User", new User());
        }
        public JsonResult Delete(int Id)
        {
            string msg = string.Empty;
            if (new ManageUser().Delete(Id))
            {
                msg = "User has been deleted successfully.";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Fetch(Paging paging, SearchDepartment departSearch)
        {
            departSearch.OrganizationId = new Account().FetchOrganization().OrganizationId;
            object gridData;
            try
            {
                List<FetchUser_Result> list = new ManageUser().FetchUsers(paging, departSearch);

                gridData = new
                {
                    data = list.ToArray(),

                    itemsCount = list.Count > 0 ? list.FirstOrDefault().TotalRecord : 0
                };


            }
            catch (Exception e)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

            return Json(gridData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Chat()
        {
            return View();
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult UserProfile()
        {
            DAL.User user = new ManageUser().FetchUserById(new Account().FetchOrganization().UserId);
            user.Active = user.IsActive.HasValue ? user.IsActive.Value : false;
            return View(user);
        }

        [HttpPost]
        public ActionResult UserProfile(DAL.User user, HttpPostedFileBase postedFiles)
        {
            string msg = string.Empty;
            //if (string.IsNullOrEmpty(user.Picture))
            //{
            //    if (postedFiles == null || postedFiles.ContentLength == 0)
            //    {
            //        TempData["Error"] = "Please upload picture.";
            //        return RedirectToAction("Index");
            //    }
            //}
            FetchOrganizationDetail_Result org = new Account().FetchOrganization();

            if (postedFiles != null && postedFiles.ContentLength > 0)
            {
                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string fileName = timestamp + System.IO.Path.GetExtension(postedFiles.FileName);
                postedFiles.SaveAs(Server.MapPath("~/App_Shared/Images/" + fileName));
                user.Picture = fileName;
                user.OriginalPicture = postedFiles.FileName;
            }
            user.OrganizationId = new Account().FetchOrganization().OrganizationId;
            user.IsActive = user.Active;
            if (new ManageUser().UpdateProfile(user))
            {
                msg = "User has been update successfully.";
            }
            return RedirectToAction("UserProfile");
        }
    }
}