﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CertiPower.Startup))]
namespace CertiPower
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
