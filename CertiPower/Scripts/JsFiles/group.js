﻿$(function () {
    BindGroupGrid(false);
});
var departSearch = {
    Name: "",
    IsActive: "",
    pageSize: 10,

};
var groupArr = [];
function BindGroupGrid(isSearch) {
    $("#groupGrid").jsGrid({
        width: "100%",

        editing: false,
        sorting: true,
        paging: true,
        autoload: true,

        pageSize: parseInt(departSearch.pageSize),
        pageButtonCount: 8,
        pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

        pagePrevText: "← Previous",
        pageNextText: "Next →",
        pageFirstText: "First",
        pageLastText: "Last",
        pageLoading: true,
        controller: {
            loadData: function (pagingParam) {
                if (isSearch) {
                    pagingParam.pageIndex = 1;
                }
                isSearch = false;

                return PostMethod("/Group/Fetch", { paging: pagingParam, departSearch: (departSearch) });
                //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

            },
        },
        //rowRenderer: function (item, index) {

        //    groupArr[index] = item;
        //    return ("<tr class='jsgrid-row'>" +
        //                        "<td class='jsgrid-control-field'>" + item.Name + "</td>" +
        //                        "<td class='jsgrid-control-field jsgrid-align-center' style='width:200px;'>" + ((item.IsActive == true) ? "<span class='label label-sm label-success'>Active</span>" : "<span class='label label-sm label-default'>In-Active</span>") + "</td>" +
        //                        "<td class='jsgrid-control-field jsgrid-align-center' style='width:200px;'><span class='glyphicon glyphicon-edit action' ng-click='DepartmentEdit(" + index + ")'></span></td>" +

        //                    "</tr>");
        //},
        fields: [
                { name: "Name", css: 'jsgrid-align-left' },
                { name: "IsActive", title: "Status", width: 200 },
                { title: "Action", sorting: false, width: 200 },
        ]

    });

}