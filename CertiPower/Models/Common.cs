﻿using CertiPower.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using BLL;

namespace CertiPower
{
    public class Account : AccountController
    {
        public DAL.FetchOrganizationDetail_Result FetchOrganization()
        {
            return System.Web.HttpContext.Current.User.Identity.GetUserId().OrganizationProfileFetch();
        }

    }
}