﻿using System.Web;
using System.Web.Optimization;

namespace CertiPower
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
             "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/Shared/js").Include(
              "~/js/jquery/jquery-2.0.3.js",
        "~/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js",
        "~/bootstrap-dist/js/bootstrap.js",
        "~/js/bootstrap-daterangepicker/moment.js",
        "~/js/bootstrap-daterangepicker/daterangepicker.js",
        "~/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js",
        "~/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.js",
        "~/js/jQuery-Cookie/jquery.cookie.js",
        //"~/js/gritter/js/jquery.gritter.js",
        "~/js/jQuery-BlockUI/jquery.blockUI.js",
        //"~/js/bootstrap-switch/bootstrap-switch.js",
        "~/js/jsgrid.js",
        "~/Scripts/CommonMethod.js",
        "~/Scripts/Method.js",
        "~/Scripts/toaster.js",
        "~/js/script.js"
        
        ));

            bundles.Add(new StyleBundle("~/Shared/css").Include(
             "~/css/cloud-admin.css",
             //"~/css/themes/night.css",
             "~/css/responsive.css",
             "~/font-awesome/css/font-awesome.css",
             "~/css/animatecss/animate.min.css",
             "~/js/jquery-todo/css/styles.css",
             //"~/js/gritter/css/jquery.gritter.css",
             "~/js/bootstrap-daterangepicker/daterangepicker-bs3.css",
             //"~/js/bootstrap-switch/bootstrap-switch.css",
             "~/css/styles.css",
             "~/css/toaster.css",
             "~/css/jsgrid-theme.css",
             "~/css/jsgrid.css",
             "~/css/Site.css"

             ));

            bundles.Add(new ScriptBundle("~/AngularJsGrid/jquery").Include(
            //"~/assets/js/jsgrid.js",
            "~/Angular/Method.js",
            "~/Scripts/angular.js",
            "~/Angular/CommonMethod.js",
             "~/Angular/Module.js"
           ));


            bundles.Add(new ScriptBundle("~/OrgShared/js").Include(
              "~/assets/js/jquery-ui.js",
        "~/assets/js/plugins/loaders/pace.js",
        "~/assets/js/core/libraries/jquery.js",
        "~/assets/js/core/libraries/bootstrap.js",
        "~/assets/js/plugins/loaders/blockui.js",
        "~/assets/js/plugins/ui/nicescroll.js",
        "~/assets/js/plugins/ui/drilldown.js",
        "~/assets/js/plugins/visualization/d3/d3.js",
        "~/assets/js/plugins/visualization/d3/d3_tooltip.js",
        "~/assets/js/plugins/forms/styling/switchery.js",
        "~/assets/js/plugins/forms/styling/uniform.js",
        "~/assets/js/plugins/forms/selects/bootstrap_multiselect.js",
        "~/assets/js/plugins/ui/moment/moment.js",
        "~/assets/js/plugins/pickers/daterangepicker.js",
        "~/Scripts/toastr.js",
        "~/assets/js/core/app.js"
        //"~/assets/js/pages/dashboard.js"
        ));

            bundles.Add(new StyleBundle("~/OrgShared/css").Include(
             "~/assets/css/jquery-ui.css",
             "~/assets/css.css",
             "~/assets/css/bootstrap.css",
             "~/assets/css/icons/icomoon/styles.css",
             "~/assets/css/bootstrap.css",
             "~/assets/css/core.css",
             "~/assets/css/components.css",
             "~/assets/css/colors.css",
             "~/assets/css/jsgrid.css",
             "~/assets/css/jsgrid-theme.css",
             "~/assets/toastr.css",
             "~/assets/Site.css"
             ));

           // bundles.Add(new ScriptBundle("~/AngularJsGrid/jquery").Include(
           // "~/assets/js/jsgrid.js",
           // "~/Angular/Method.js",
           // "~/Scripts/angular.js",
           // "~/Angular/CommonMethod.js",
           //  "~/Angular/Module.js"
           //));
        }
    }
}
