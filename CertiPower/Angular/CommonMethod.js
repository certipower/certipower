﻿$(function () {
    //toastr.options = {
    //    "closeButton": false,
    //    "debug": false,
    //    "newestOnTop": false,
    //    "progressBar": true,
    //    "positionClass": "toast-bottom-full-width",//"toast-top-full-width",//"toast-bottom-right",
    //    "preventDuplicates": false,
    //    "onclick": null,
    //    "showDuration": "3000",
    //    "hideDuration": "1000",
    //    "timeOut": "10000",
    //    "extendedTimeOut": "1000",
    //    "showEasing": "swing",
    //    "hideEasing": "linear",
    //    "showMethod": "fadeIn",
    //    "hideMethod": "fadeOut"
    //}


    setInterval(function GetCurrentTime() {

        var time = new Date();
        $(".current-time").val((time.getHours().toString().length < 2 ? "0" + time.getHours() : time.getHours()) + " : " + (time.getMinutes().toString().length < 2 ? "0" + time.getMinutes() : time.getMinutes()) + " : " + (time.getSeconds().toString().length < 2 ? "0" + time.getSeconds() : time.getSeconds()));

    }, 1000);


});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isFloat(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    } else {
        // If the number field already has . then don't allow to enter . again.
        if (evt.target.value.search(/\./) > -1 && charCode == 46) {
            return false;
        }
        return true;
    }
}
function isLength(evt) {
    //$("#length").attr('maxlength', '2');
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {


        return false;
    } else {

        // If the number field already has . then don't allow to enter . again.
        if (evt.target.value.search(/\./) > -1 && charCode == 46) {
            $("#length").attr('maxlength', '4');

            return false;
        }
        else if (charCode == 46) {

            $("#length").attr('maxlength', '4');

        }
        return true;
    }
}

$('input').bind('keypress', function (event) {

    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        event.preventDefault();
        return false;
    }
});

LoaderShow = function () {
    $("#divLoader").removeClass("showNone");
}
LoaderHide = function () {
    $("#divLoader").addClass("showNone");
}
GetAccountCode = function () {
    return $('#accountCode').val();
}
SetAccountCode = function (accountCode) {
    $('#accountCode').val(accountCode);
}
$(function () {
    $('.accountCodeTextBox').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Generic/FetchAccounts",
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {

                    response($.map(data, function (item) {

                        return {
                            label: item.Name,
                            val: item.Code
                        }
                    }))

                },
                error: function (response) {
                    // alert(response.responseText);
                },
                failure: function (response) {
                    //  alert(response.responseText);
                }
            });
        },

        change: function (event, ui) {
            if (ui.item == null || ui.item == undefined) {
                $(".accountCodeTextBox").val("");
                $('#accountCode').val("");

            }
        },

        select: function (e, i) {
            $('#accountCode').val(i.item.val);

        },
        minLength: 1
    });

    $('.productCodeTextBox').autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Generic/FetchProducts",
                data: "{ 'prefix': '" + request.term + "'}",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: item.Name,
                            val: item.Code
                        }
                    }))

                },
                error: function (response) {
                },
                failure: function (response) {
                }
            });
        },

        change: function (event, ui) {
            if (ui.item == null || ui.item == undefined) {
                $(".productCodeTextBox").val("");
                $('#productSKU').val("");

            }
        },

        select: function (e, i) {
            $('#productSKU').val(i.item.val);

        },
        minLength: 1
    });
});
$('.number').keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
      (text.substring(text.indexOf('.')).length > 2) &&
      (event.which != 0 && event.which != 8)) {
        event.preventDefault();
    }
});
$(document).on('keydown', '.neg', function (e) {


    if (!((e.keyCode > 95 && e.keyCode < 106)
     || (e.keyCode > 47 && e.keyCode < 58)
     || e.keyCode == 8)) {

        return false;

    }

    //else if ($('.neg').val().length > 2) {

    //    return false;
    //}


})
$(document).on('keydown', '.length', function (e) {
    var data = $('.length').val();


    //if (!((e.keyCode > 95 && e.keyCode < 106)
    // || (e.keyCode > 47 && e.keyCode < 58)
    // || e.keyCode == 8)) {

    //    return false;

    //}

    //else if ($('.neg').val().length > 2) {

    //    return false;
    //}


})
$(document).on("keypress", ".jsgrid-align-Quantity input", function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
      (text.substring(text.indexOf('.')).length > 2) &&
      (event.which != 0 && event.which != 8)) {
        event.preventDefault();
    }
});
$(function () {
    $('.datepicker').datepicker({
        changeMonth: true,
        dateFormat: 'mm/dd/yy',
        changeYear: true,
        day: 0
    });
    // $('.datepicker').before("<img class='datepicker-close' onclick='ClearDate(this);' src='http://www.amazingsidingstl.com/wp-content/themes/asw/images/delete.png'/>");
});

$(document).on("keypress", ".decimal", function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
      ((event.which < 48 || event.which > 57) &&
        (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((text.indexOf('.') != -1) &&
      (text.substring(text.indexOf('.')).length > 2) &&
      (event.which != 0 && event.which != 8)) {
        event.preventDefault();
    }
});
$(document).on("keypress", ".datepicker", function (event) {
    return false;
});
function ConvertToDecimal(value) {
    value = value.toString();
    if (value.indexOf(".") >= 0) {
        var floatValue = value.substring(0, value.indexOf(".") + 3);
        return floatValue;
    }
    else {
        return value;
    }
};
function Print() {
    var prtContent = document.getElementById('DivPrint');
    var WinPrint = window.open('', '', 'left=0,top=0,width=780,height=600,toolbar=0,scrollbars=1,status=1');
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
    prtContent.innerHTML = strOldOne;

};
function ClearDate(domObj) {
    $(domObj.nextElementSibling).val("");
}

function SuccessMessage(msg) {
    toastr.success(msg);
}

function ErrorMessage(msg) {
    toastr.error(msg);
}
function WarningMessage(msg) {
    toastr.warning(msg);
}
function InfoMessage(msg) {
    toastr.info(msg);
}
function ShowMessage(msg) {
    if (msg.Info) {
        InfoMessage(msg.MessageDetail);
    }
    else if (msg.Warning) {
        WarningMessage(msg.MessageDetail);
    }
    else if (msg.Success) {
        SuccessMessage(msg.MessageDetail);
    }
    else if (!msg.Success) {
        ErrorMessage(msg.MessageDetail);
    }
    else if (msg.Info) {
        InfoMessage(msg.MessageDetail);
    }
    else if (msg.Warning) {
        WarningMessage(msg.MessageDetail);
    }
}

$(document).on("keypress", ".numeric-whole", function (evt) {
    return isNumber(evt);
});

$(document).on("keypress", ".numeric-float", function (evt) {
    return isFloat(evt);
});

GetProductSKU = function () {
    return $('#productSKU').val();
}
SetProductSKU = function (productSKU) {
    $('#productSKU').val(productSKU);
}

