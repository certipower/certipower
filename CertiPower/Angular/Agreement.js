﻿app.controller("agreementCTRL", function ($scope, $compile, $timeout, $http) {
    var departmentArr = [];

    var depart = {
        Name: "",
        Description: "",
        IsActive: "",
        Id: ""

    };

    var departSearch = {
        Name: "",
        Status: "",
        pageSize: 10,

    };
    $scope.depart = depart;
    $scope.departSearch = departSearch;

    DepartmentBindGrid(false);

    $scope.DepartmentSave = function () {
        $("#FolderModal").modal('hide');
        var response = $http({
            method: "post",
            url: "/Organization/Save",
            data: JSON.stringify(depart),
            dataType: "json"
        });
        response.then(function (msg) {
            DepartmentBindGrid(false);
            //ShowMessage(msg.data);
        });

    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.EditOrganization = function (rowNumber) {
        args = departmentArr[rowNumber];
        location.href = "/Agreement/AddAgreement?Id=" + args.Id;
    }

    $scope.DeleteFolder = function (rowNumber) {
        if (confirm('Are you sure to delete this agreement?')) {
            args = departmentArr[rowNumber];
            var response = $http({
                method: "post",
                url: "/Agreement/Delete",
                data: JSON.stringify(args),
                dataType: "json"
            });
            response.then(function (msg) {
                DepartmentBindGrid(false);
                // ShowMessage(msg.data);
            });
        }
    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.Search = function () {
        DepartmentBindGrid(true)
    };
    $scope.DepartmentChangePageSize = function () {

        DepartmentBindGrid(true);
    }

    function DepartmentBindGrid(isSearch) {
        $("#agreementGrid").jsGrid({
            width: "100%",

            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: parseInt(departSearch.pageSize),
            pageButtonCount: 8,
            pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

            pagePrevText: "← Previous",
            pageNextText: "Next →",
            pageFirstText: "First",
            pageLastText: "Last",
            pageLoading: true,
            controller: {
                loadData: function (pagingParam) {
                    if (isSearch) {
                        pagingParam.pageIndex = 1;
                    }
                    isSearch = false;

                    return PostMethod("/Agreement/Fetch", { paging: pagingParam, departSearch: ($scope.departSearch) });
                    //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

                },
            },
            rowRenderer: function (item, index) {

                departmentArr[index] = item;
                return $compile("<tr class='jsgrid-row'>" +
                                    "<td class='jsgrid-control-field'>" + item.PCFullName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.PCEmail + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.PCMobileNo + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.ProgramName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.ProgramType + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'>" + item.TraineeNumber + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'><a class='btn btn-sm btn-success' ng-click='EditOrganization(" + index + ")'><i class='fa fa-pencil-square-o'></i></a></td>" +
// <span class='btn btn-sm btn-success'  ng-click='DeleteFolder(" + index + ")'><i class='fa fa-trash-o'></i></span>
                                "</tr>")($scope);
            },
            fields: [
                    { name: "PCFullName", title: "Name", css: 'jsgrid-align-left' },
                    { name: "PCEmail", title: "Email", css: 'jsgrid-align-left' },
                    { name: "PCMobileNo", title: "Mobile No", css: 'jsgrid-align-left' },
                    { name: "ProgramName", title: "Program Name", css: 'jsgrid-align-left' },
                    { name: "ProgramType", title: "Program Type", css: 'jsgrid-align-left' },
                    { name: "TraineeNumber", title: "No Of Students",align:"center" },
                    { title: "Action", sorting: false },
            ]

        });

    }

});