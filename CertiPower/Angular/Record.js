﻿app.controller("RecordCTRL", ['$scope', '$compile', '$timeout', '$http', function ($scope, $compile, $timeout, $http) {
    var departmentArr = [];

    var depart = {
        FolderId: "",
        FileName: "",
        FileNameStr: "",
        FileType: "",
        FileDescription: "",
        InvoiceAmount: "",
        Priority: "",
        ReceiveFrom: "",
        SentTo: "",
        Notes: "",
        Id: ""

    };

    var departSearch = {
        Name: "",
        FolderId: "",
        pageSize: 10,

    };
    $scope.depart = depart;
    $scope.departSearch = departSearch;
    $http.get("/Generic/FetchFolders").then(function (result) {
        $scope.folders = result.data;
        $scope.searchFolder = result.data;
    });
    DepartmentBindGrid(false);
    $scope.DepartmentOpenModel = function () {
        LoaderShow();
        $.ajax({
            url: "/Record/OpenModal",
            type: "get",
            data: {},
            success: function (result) {
                $("#dvRecordDetail").html(result);
                $('#RecordModal').modal('show');
                LoaderHide();
            }
        });

        //$("#RecordModal").modal('show');
    };


    $scope.DepartmentSave = function () {
        $("#RecordModal").modal('hide');
        $scope.depart.FileNameStr = $scope.myFile;
        var response = $http({
            method: "post",
            url: "/Record/Save",
            data: JSON.stringify($scope.depart),
            dataType: "json"
        });
        response.then(function (msg) {
            DepartmentBindGrid(false);
            ShowMessage(msg.data);
        });

    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.DeleteRecord = function (rowNumber) {
        if (confirm('Are you sure to delete the Record?')) {
            args = departmentArr[rowNumber];
            var response = $http({
                method: "post",
                url: "/Record/Delete",
                data: JSON.stringify(args),
                dataType: "json"
            });
            response.then(function (msg) {
                DepartmentBindGrid(false);
                ShowMessage(msg.data);
            });
        }
    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.Search = function () {
        DepartmentBindGrid(true)
    };
    $scope.DepartmentChangePageSize = function () {

        DepartmentBindGrid(true);
    }
    $scope.DepartmentEdit = function (rowNumber) {
        args = departmentArr[rowNumber];
        LoaderShow();
        $.ajax({
            url: "/Record/FetchById",
            type: "get",
            data: { recordId: args.Id },
            success: function (result) {
                $("#dvRecordDetail").html(result);
                $('#RecordModal').modal('show');
                LoaderHide();
            }
        });
    }
    function DepartmentBindGrid(isSearch) {
        $("#recordGrid").jsGrid({
            width: "100%",

            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: parseInt(departSearch.pageSize),
            pageButtonCount: 8,
            pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

            pagePrevText: "← Previous",
            pageNextText: "Next →",
            pageFirstText: "First",
            pageLastText: "Last",
            pageLoading: true,
            controller: {
                loadData: function (pagingParam) {
                    if (isSearch) {
                        pagingParam.pageIndex = 1;
                    }
                    isSearch = false;

                    return PostMethod("/Record/Fetch", { paging: pagingParam, departSearch: ($scope.departSearch) });
                    //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

                },
            },
            rowRenderer: function (item, index) {

                departmentArr[index] = item;
                return $compile("<tr class='jsgrid-row'>" +
                                    "<td class='jsgrid-control-field'><a href='/Record/DownLoadFile?file="+item.FileName+"'>" + item.OriginalFileName + "</a></td>" +
                                    "<td class='jsgrid-control-field'>" + item.FolderName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + ((item.Priority == 'Critical') ? "<span class='label label-sm label-danger'>" + item.Priority + "</span>" : (item.Priority == 'Medium') ? "<span class='label label-sm label-primary'>" + item.Priority + "</span>" : (item.Priority == "Low") ? "<span class='label label-sm label-success'>" + item.Priority + "</span>" : "<span class='label label-sm label-warning'>" + item.Priority + "</span>") + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'>" + item.ReceiveFrom + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.SentTo + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'><span class='btn btn-sm btn-success' ng-click='DepartmentEdit(" + index + ")'><i class='fa fa-pencil-square-o'></i></span> <span class='btn btn-sm btn-success' ng-click='DeleteRecord(" + index + ")'><i class='fa fa-trash-o'></i></span></td>" +

                                "</tr>")($scope);
            },
            fields: [
                    { name: "OriginalFileName", title: "Name", css: 'jsgrid-align-left' },
                    { name: "FolderName", title: "Folder", css: 'jsgrid-align-left' },
                    { name: "Priority", css: 'jsgrid-align-left' },
                    { name: "ReceiveFrom", title: "Received From", align: "center" },
                    { name: "SentTo", title: "Sent To", css: 'jsgrid-align-left' },
                    { title: "Action", sorting: false },
            ]

        });

    }
    $scope.DepartmentClearFields = function () {
        $scope.depart = [];
        $('#RecordModal').modal('show');
    }

}]);

