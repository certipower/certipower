﻿
app.controller("UserCTRL", function ($scope, $compile, $timeout, $http) {
    var departmentArr = [];

    var depart = {
        FullName: "",
        Email: "",
        Password: "",
        Phone: "",
        Gender: "M",
        Nationality: "",
        JobTitle: "",
        PictureStr: "",
        Picture: "",
        Notes: "",
        DateOfBirthStr: "",
        IsActive: "",
        GroupId: "",
        Id: ""

    };

    var departSearch = {
        Name: "",
        GroupId: "",
        Status:"",
        pageSize: 10,

    };
    $scope.depart = depart;
    $scope.departSearch = departSearch;
    $http.get("/Generic/FetchGroups").then(function (result) {
        $scope.groups = result.data;
        $scope.SearchGroups = result.data;
    });
    DepartmentBindGrid(false);
    $scope.DepartmentOpenModel = function () {
        LoaderShow();
        $.ajax({
            url: "/User/OpenModal",
            type: "get",
            data: {},
            success: function (result) {
                $("#dvUserDetail").html(result);
                $('#UserModal').modal('show');
                LoaderHide();
            }
        });
    };


    $scope.DepartmentSave = function () {
        $("#UserModal").modal('hide');
        var response = $http({
            method: "post",
            url: "/User/Save",
            data: JSON.stringify(depart),
            dataType: "json"
        });
        response.then(function (msg) {
            DepartmentBindGrid(false);
            ShowMessage(msg.data);
        });

    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.DeleteUser = function (rowNumber) {
        if (confirm('Are you sure to delete the User?')) {
            args = departmentArr[rowNumber];
            var response = $http({
                method: "post",
                url: "/User/Delete",
                data: JSON.stringify(args),
                dataType: "json"
            });
            response.then(function (msg) {
                DepartmentBindGrid(false);
                ShowMessage(msg.data);
            });
        }
    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.Search = function () {
        DepartmentBindGrid(true)
    };
    $scope.DepartmentChangePageSize = function () {

        DepartmentBindGrid(true);
    }
    $scope.DepartmentEdit = function (rowNumber) {
        LoaderShow();
        args = departmentArr[rowNumber];
        $.ajax({
            url: "/User/FetchById",
            type: "get",
            data: { userId: args.Id },
            success: function (result) {
                $("#dvUserDetail").html(result);
                $('#UserModal').modal('show');
                LoaderHide();
            }
        });

    }
    function DepartmentBindGrid(isSearch) {
        $("#userGrid").jsGrid({
            width: "100%",

            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: parseInt(departSearch.pageSize),
            pageButtonCount: 8,
            pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

            pagePrevText: "← Previous",
            pageNextText: "Next →",
            pageFirstText: "First",
            pageLastText: "Last",
            pageLoading: true,
            controller: {
                loadData: function (pagingParam) {
                    if (isSearch) {
                        pagingParam.pageIndex = 1;
                    }
                    isSearch = false;

                    return PostMethod("/User/Fetch", { paging: pagingParam, departSearch: ($scope.departSearch) });
                    //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

                },
            },
            rowRenderer: function (item, index) {

                departmentArr[index] = item;
                return $compile("<tr class='jsgrid-row'>" +
                                    "<td class='jsgrid-control-field'>" + item.FullName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.GroupName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.Email + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'>" + item.Phone + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.Nationality + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'>" + ((item.IsActive == true) ? "<span class='label label-sm label-success'>Active</span>" : "<span class='label label-sm label-default'>In-Active</span>") + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'><span class='btn btn-sm btn-success' ng-click='DepartmentEdit(" + index + ")'><i class='fa fa-pencil-square-o' ></i></span> <span class='btn btn-sm btn-success' ng-click='DeleteUser(" + index + ")'><i class='fa fa-trash-o' ></i></span></td>" +

                                "</tr>")($scope);
            },
            fields: [
                    { name: "FullName", title: "Name", css: 'jsgrid-align-left' },
                    { name: "GroupName", title: "Group", css: 'jsgrid-align-left' },
                    { name: "Email", css: 'jsgrid-align-left' },
                    { name: "Phone", align: "center" },
                    { name: "Nationality", css: 'jsgrid-align-left' },
                    { name: "IsActive", title: "Status" },
                    { title: "Action", sorting: false },
            ]

        });

    }
    $scope.DepartmentClearFields = function () {
        $scope.depart = [];
        $('#UserModal').modal('show');
    }

});

