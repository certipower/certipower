﻿app.controller("InstituteCTRL", function ($scope, $compile, $timeout, $http) {
    var departmentArr = [];

    var depart = {
        Name: "",
        Description: "",
        IsActive: "",
        Id: ""

    };

    var departSearch = {
        Name: "",
        Status: "",
        pageSize: 10,

    };
    $scope.depart = depart;
    $scope.departSearch = departSearch;

    DepartmentBindGrid(false);

    $scope.DepartmentSave = function () {
        $("#FolderModal").modal('hide');
        var response = $http({
            method: "post",
            url: "/Organization/Save",
            data: JSON.stringify(depart),
            dataType: "json"
        });
        response.then(function (msg) {
            DepartmentBindGrid(false);
            //ShowMessage(msg.data);
        });

    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.EditOrganization = function (rowNumber) {
        args = departmentArr[rowNumber];
        location.href = "/Institute/AddInstitute?Id=" + args.Id;
    }

    $scope.DeleteFolder = function (rowNumber) {
        if (confirm('Are you sure to delete the organization?')) {
            args = departmentArr[rowNumber];
            var response = $http({
                method: "post",
                url: "/Organization/Delete",
                data: JSON.stringify(args),
                dataType: "json"
            });
            response.then(function (msg) {
                DepartmentBindGrid(false);
                // ShowMessage(msg.data);
            });
        }
    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.Search = function () {
        DepartmentBindGrid(true)
    };
    $scope.DepartmentChangePageSize = function () {

        DepartmentBindGrid(true);
    }

    function DepartmentBindGrid(isSearch) {
        $("#instituteGrid").jsGrid({
            width: "100%",

            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: parseInt(departSearch.pageSize),
            pageButtonCount: 8,
            pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

            pagePrevText: "← Previous",
            pageNextText: "Next →",
            pageFirstText: "First",
            pageLastText: "Last",
            pageLoading: true,
            controller: {
                loadData: function (pagingParam) {
                    if (isSearch) {
                        pagingParam.pageIndex = 1;
                    }
                    isSearch = false;

                    return PostMethod("/Institute/Fetch", { paging: pagingParam, departSearch: ($scope.departSearch) });
                    //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

                },
            },
            rowRenderer: function (item, index) {

                departmentArr[index] = item;
                return $compile("<tr class='jsgrid-row'>" +
                                    "<td class='jsgrid-control-field'>" + item.OrganizationName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.OrganizationType + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.OrganizationSize + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.BankName + "</td>" +
                                    "<td class='jsgrid-control-field'>" + item.ZipCode + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'>" + ((item.IsActive == true) ? "<span class='label label-sm label-success'>Active</span>" : "<span class='label label-sm label-default'>In-Active</span>") + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center'><a class='btn btn-sm btn-success' ng-click='EditOrganization(" + index + ")'><i class='fa fa-pencil-square-o'></i></a> </td>" +

                                    //<span class='btn btn-sm btn-success'  ng-click='DeleteFolder(" + index + ")'><i class='fa fa-trash-o'></i></span>

                                "</tr>")($scope);
            },
            fields: [
                    { name: "OrganizationName", title: "Name", css: 'jsgrid-align-left' },
                    { name: "OrganizationType", title: "Type", css: 'jsgrid-align-left' },
                    { name: "OrganizationSize", title: "Size", css: 'jsgrid-align-left' },
                    { name: "BankName", title: "Bank Name", css: 'jsgrid-align-left' },
                    { name: "ZipCode", title: "Zip Code", css: 'jsgrid-align-left' },
                    { name: "IsActive", title: "Status", css: 'jsgrid-align-center' },
                    { title: "Action", sorting: false },
            ]

        });

    }

});