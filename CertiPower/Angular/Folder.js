﻿
app.controller("FolderCTRL", function ($scope, $compile, $timeout, $http) {
    var departmentArr = [];

    var depart = {
        Name: "",
        Description: "",
        IsActive: "",
        Id: ""

    };

    var departSearch = {
        Name: "",
        Status: "",
        pageSize: 10,

    };
    $scope.depart = depart;
    $scope.departSearch = departSearch;

    DepartmentBindGrid(false);
    
    $scope.DepartmentOpenModel = function () {
        $scope.heading = "Add";
        $scope.btnSave = "Save";

        $scope.depart.IsActive = true;
        $("#FolderModal").modal('show');
        $scope.DepartmentClearFields();
    };


    $scope.DepartmentSave = function () {
        $("#FolderModal").modal('hide');
        var response = $http({
            method: "post",
            url: "/Folder/Save",
            data: JSON.stringify(depart),
            dataType: "json"
        });
        response.then(function (msg) {
            DepartmentBindGrid(false);
            //ShowMessage(msg.data);
        });

    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.DeleteFolder = function (rowNumber) {
        if (confirm('Are you sure to delete the folder?')) {
            args = departmentArr[rowNumber];
            var response = $http({
                method: "post",
                url: "/Folder/Delete",
                data: JSON.stringify(args),
                dataType: "json"
            });
            response.then(function (msg) {
                DepartmentBindGrid(false);
               // ShowMessage(msg.data);
            });
        }
    }, function () {
        $scope.message = 'Error in updating Department record';
    };

    $scope.Search = function () {
        DepartmentBindGrid(true)
    };
    $scope.DepartmentChangePageSize = function () {

        DepartmentBindGrid(true);
    }
    $scope.DepartmentEdit = function (rowNumber) {
        args = departmentArr[rowNumber];
        $scope.heading = "Update";
        $scope.btnSave = "Update";
        $scope.depart.Id = args.Id;
        $scope.depart.Name = args.Name;
        $scope.depart.IsActive = args.IsActive;
        $scope.depart.Description = args.Description;

        $('#FolderModal').modal('show');
    }
    function DepartmentBindGrid(isSearch) {
        $("#folderGrid").jsGrid({
            width: "100%",

            editing: false,
            sorting: true,
            paging: true,
            autoload: true,

            pageSize: parseInt(departSearch.pageSize),
            pageButtonCount: 8,
            pagerFormat: "{first} {prev} {pages} {next} {last} - Page {pageIndex} of {pageCount}",   //- Page {pageIndex} of {pageCount}

            pagePrevText: "← Previous",
            pageNextText: "Next →",
            pageFirstText: "First",
            pageLastText: "Last",
            pageLoading: true,
            controller: {
                loadData: function (pagingParam) {
                    if (isSearch) {
                        pagingParam.pageIndex = 1;
                    }
                    isSearch = false;

                    return PostMethod("/Folder/Fetch", { paging: pagingParam, departSearch: ($scope.departSearch) });
                    //  return PostMethod("/Department/Fetch", JSON.stringify({ pagingParam: paging,depart:}));

                },
            },
            rowRenderer: function (item, index) {

                departmentArr[index] = item;
                return $compile("<tr class='jsgrid-row'>" +
                                    "<td class='jsgrid-control-field'>" + item.Name + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center' style='width:200px;'>" + ((item.IsActive == true) ? "<span class='label label-sm label-success'>Active</span>" : "<span class='label label-sm label-default'>In-Active</span>") + "</td>" +
                                    "<td class='jsgrid-control-field jsgrid-align-center' style='width:200px;'><span class='btn btn-sm btn-success'><i class='fa fa-pencil-square-o' ng-click='DepartmentEdit(" + index + ")'></i></span> <span class='btn btn-sm btn-success'><i class='fa fa-trash-o' ng-click='DeleteFolder(" + index + ")'></i></span></td>" +

                                "</tr>")($scope);
            },
            fields: [
                    { name: "Name", css: 'jsgrid-align-left' },
                    { name: "IsActive", title: "Status", width: 200 },
                    { title: "Action", sorting: false, width: 200 },
            ]

        });

    }
    $scope.DepartmentClearFields = function () {
        $scope.depart.Name = "";
        $scope.depart.Id = 0;
        $scope.depart.Description = "";
        $('#FolderModal').modal('show');
    }

});

